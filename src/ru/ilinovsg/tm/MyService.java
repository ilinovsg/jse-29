package ru.ilinovsg.tm;

import java.util.ArrayList;
import java.util.List;

public class MyService {
    public long sum(String arg1, String arg2) throws IllegalArgumentException {
        return Long.parseLong(arg1) + Long.parseLong(arg2);
    }

    public long factorial(String arg) throws ArithmeticException,IllegalArgumentException {
        if (Long.parseLong(arg) < 0) {
            throw new IllegalArgumentException();
        }
        if (Long.parseLong(arg) == 0) {
            return 0;
        }
        long result = 1;
        for (int i = 1; i <= Long.parseLong(arg); i++) {
            result = Math.multiplyExact(result, i);
        }
        return result;
    }

    public long[] fibonacci(String arg) throws IllegalArgumentException, ArithmeticException {
        if (Long.parseLong(arg) < 0) {
            throw new IllegalArgumentException();
        }
        Math.incrementExact(Long.parseLong(arg));
        List<Long> list = new ArrayList<>();
        if (Long.parseLong(arg) == 0) {
            list.add(Long.parseLong(arg));
        }
        else {
            long n0 = 1, n1 = 1, n2;
            int index = 2;
            list.add(n0);
            list.add(n1);
            while((n0 + n1) <= Long.parseLong(arg)) {
                n2 = Math.addExact(n0, n1);
                n0 = n1;
                n1 = n2;
                if ((n0 + n1) > Long.parseLong(arg) &&
                     n2 != Long.parseLong(arg)) {
                    throw new IllegalArgumentException();
                }
                list.add(n2);
                index++;
            }
        }
        return  list.stream().mapToLong(l -> l).toArray();
    }
}
