package ru.ilinovsg.tm;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        MyService myService = new MyService();

        System.out.println(myService.sum("1", "3"));

        System.out.println(myService.factorial("5"));

        for (Long item : myService.fibonacci("11")) {
            System.out.print(item+" ");
        }
    }
}
